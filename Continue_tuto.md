# <div align="center"> <img src="images/header_2.png" alt="Logo"> </div>


# Using continue like copilot with ISDM Chat


## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contact](#contact)


## Installation

1. Install continue.dev plugin in VSCodium or VSCode
2. Get your API key on https://isdm-chat.crocc.meso.umontpellier.fr/ , click on your profile at the bottom left, then Settings, Account, API keys
3. In your home in .continue/config.json set configuration from the file continue-config.json (in this git), set your token.\
   You also may access the config.json by clicking on the gear icon "Configure Continue", viewable after selecting the Continue icon in your VSCode.


You may fill the config file by referring to the [continue-config.json](continue-config.json) file available in the repo. 
 

## Usage

You may use it as a ChatBot inside VSCode, by clicking on the Continue icon and conversing with it.\
You can also ask with "CTRL + i" on part of your code, or just wait for auto-completion proposal.


## Contact

drocc-crocc-contact-request <drocc-crocc-contact-request@groupes.renater.fr>
