# <div align="center"> <img src="images/header_2.png" alt="Logo"> </div>


# Using the ISDM Chat


## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Features](#features)
    - [Documents](#documents)
    - [WebSearch](#websearch)
- [Infrastructure](#infrastructure)
- [Contact](#contact)


## Installation

Dependencies 

```
pip install --upgrade pip
pip install -r requirements.txt
``` 

JWT token and API Key are available in your [CHAT ISDM](https://isdm-chat.crocc.meso.umontpellier.fr/) account, in the Settings/Account/API Key tab.

Two models at your disposal : 

-  Mixtral 8 x 7b (Ollama) ("mixtral:8x7b-instruct-v0.1-q5_0")
-  Codestral 22b (OpenAI) ("solidrust/Codestral-22B-v0.1-hf-AWQ")

Access URLs :

- Ollama : "https://chat.crocc.meso.umontpellier.fr/ollama"

- OpenAI : "https://isdm-chat.crocc.meso.umontpellier.fr/openai"

## Usage

1. [Ollama API](OllamaAPI.py)

2. [OpenAI API](OpenaiAPI.py)

3. Using the ISDM Chat as a Copilot in VSCode with [Continue](Continue_tuto.md) 

## Features

### Documents

A RAG feature has been added, enabling users to add documents that can be queried through ISDM Chat.\
For the moment, the only way to add documents is by clicking on the "+" icon to the left of the chat bar.\
It will not be saved on your profile, and you'll also have to upload it again for each request.

Once the document is added, you can make a query on it.

### WebSearch 

Web search is enabled, allowing the LLM to enrich its response through an Internet search.\
For the time being, a maximum of 3 search results are returned.\
To do so, check the option that appears by clicking on the "+" icon to the left of the chat bar.

## Infrastructure

It's hosted on CROCC and in beta test.\
Account are only approved for institutional email.

## Contact

<drocc-crocc-contact-request@groupes.renater.fr>

For any questions about the tool, feel free to email us here : 

- <gino.frazzoli@umontpellier.fr>
- <arsene.fougerouse@umontpellier.fr>

