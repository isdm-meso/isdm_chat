from langchain_community.chat_models import ChatOllama
from langchain_core.messages import HumanMessage
import os
from dotenv import load_dotenv

load_dotenv()

LLM_MODEL = os.getenv("LLM_MODEL")
LLM_JWT_BEARER = os.getenv("LLM_JWT_BEARER")
LLM_API_URL = os.getenv("LLM_API_URL")

llm = ChatOllama(model=LLM_MODEL, base_url=LLM_API_URL,
headers={"Authorization": "Bearer " + LLM_JWT_BEARER,"Content-Type":"application/json",})

texte_question = "ChatBot, mon gentil ChatBot, quels sont les chiffres du prochain Loto ?"

messages = [HumanMessage(content=texte_question)]

chat_model_response = llm.invoke(messages)
print(chat_model_response.content)