from langchain_core.messages import HumanMessage
from langchain_openai import ChatOpenAI
import os

OPENAI_API_KEY = os.getenv("LLM_API_KEY")
OPENAI_CHAT_MODEL = os.getenv("LLM_MODEL")
OPENAI_CHAT_API_URL = os.getenv("LLM_API_URL")

llm = ChatOpenAI(
    model=OPENAI_CHAT_MODEL,
    openai_api_key=OPENAI_API_KEY,
    openai_api_base=OPENAI_CHAT_API_URL)

texte_question = "Type your query here"

messages = [HumanMessage(content=texte_question)]
chat_model_response = llm.invoke(messages)
print(chat_model_response.content)